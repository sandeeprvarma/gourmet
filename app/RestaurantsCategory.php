<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantsCategory extends Model
{
    protected $table = 'restaurant_category';
    protected $fillable = ['name','master_category','logo', 'c_logo', 'description'];
}
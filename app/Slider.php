<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders';
    public $timestamps = false;
    protected $fillable = ['image', 'desc', 'slider_type', 'rank'];
}

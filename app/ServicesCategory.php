<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicesCategory extends Model
{
    protected $table = 'services_category';
    protected $fillable = ['name','logo'];
}

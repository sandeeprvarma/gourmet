<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'booking';
    protected $filable = ['name','email','phone','desc','destination','bk_date','hour','mins','meridian','pickup'];
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($user = Auth::user()) {
            if($user->user_type != 1)
                return redirect('/');
            // if($user->user_type == 2){
            //     return redirect('/');
            // }elseif($user->user_type == 1) {
            //     return redirect('/admin');
            // }else {
            //     return redirect('/login');
            // }
        }else {
            return redirect('/login');
        }
        return $next($request);
    }
}

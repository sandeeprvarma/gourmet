<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicesCategory;
use App\SuppliersData;
use App\Slider;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$services = ServicesCategory::all();
    	$sliders = Slider::where('slider_type',3)->orderBy('rank')->get();
        return view('services')->with(['services' => $services, 'sliders' => $sliders]);
    }

    public function serviceProviders($id)
    {
    	$services = ServicesCategory::query()->select(['id','name'])->where('id', $id)->first();
    	$suppliers_list = SuppliersData::where('parent_cat', $id)->get();
        return view('supplier-list')->with(['suppliers_list' => $suppliers_list, 'services' => $services]);
    }

    public function supplierInfo($id) {
    	$supplier_data = SuppliersData::where('id', $id)->first();
        return view('load_supplier')->with('supplier_data',$supplier_data);
    }
}

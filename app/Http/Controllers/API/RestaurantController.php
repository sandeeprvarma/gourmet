<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Restaurants;
use App\Category;
use App\RestaurantsCategory;
use Illuminate\Support\Facades\Validator;

class RestaurantController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRestaurantsListById($id)
    {
        $rest = Restaurants::where('cat_id', $id)->get();
        if (empty($rest->toArray())) {
            return $this->sendError('Restaurant not found.');
        }
        return $this->sendResponse($rest->toArray(), 'Restaurants retrieved successfully.');
    }

    public function getRestaurants()
    {
        $rest = Restaurants::all();
        if (empty($rest->toArray())) {
            return $this->sendError('Restaurant not found.');
        }
        return $this->sendResponse($rest->toArray(), 'Restaurants retrieved successfully.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllCategories()
    {
        $category = Category::all();
        if (empty($category->toArray())) {
            return $this->sendError('No category found.');
        }
        return $this->sendResponse($category->toArray(), 'Categories retrieved successfully.');   
    }

    public function getRestaurantsByCategory($id)
    {
        $rest = RestaurantsCategory::where('master_category', $id)->get();
        if (empty($rest->toArray())) {
            return $this->sendError('Restaurant\'s list not found.');
        }
        return $this->sendResponse($rest->toArray(), 'Restaurant\'s list retrieved successfully.');
    }

    public function searchResult(Request $request) {
        $search_result = array();
        $count = 0;
        if(empty($request->search)) {
            return $this->sendError('Please type a valid search keyword.');    
        }
        $category_data = Category::where('name', 'LIKE', "%{$request->search}%")->get();
        if(!$category_data->isEmpty()) {
            foreach ($category_data as $key => $category) {
                if($category->id == 1) {
                    $sub_cat = RestaurantsCategory::all();
                    foreach ($sub_cat as $skey => $sc) {
                        $restaurant_data = Restaurants::query()->select(['id','name', 'image', 'description', 'site_url','master_cat','cat_id'])->where(['master_cat' => $category->id, 'cat_id' => $sc->id])->get();
                        if(!$restaurant_data->isEmpty()) {
                            $count += count($restaurant_data);
                            array_push($search_result, $restaurant_data);
                        }
                    }
                } else {
                    $restaurant_data = Restaurants::query()->select(['id','name', 'image', 'description', 'site_url','master_cat','cat_id'])->where('master_cat',$category->id)->get();
                    if(!$restaurant_data->isEmpty()) {
                        $count += count($restaurant_data);
                        array_push($search_result, $restaurant_data);
                    }
                }
            }
        }
        $restaurant = Restaurants::where('name', 'LIKE', "%{$request->search}%")->get();
        if(!$restaurant->isEmpty()) {
            $count += count($restaurant);
            array_push($search_result, $restaurant);
        }

        $restaurant_cat = RestaurantsCategory::where('name', 'LIKE', "%{$request->search}%")->get();
        if(!$restaurant_cat->isEmpty()) {
            foreach ($restaurant_cat as $skey => $sc) {
                $restaurant_data = Restaurants::query()->select(['id','name', 'image', 'description', 'site_url','master_cat','cat_id'])->where(['master_cat' => $category->id, 'cat_id' => $sc->id])->get();
                if(!$restaurant_data->isEmpty()) {
                    $count += count($restaurant_data);
                    array_push($search_result, $restaurant_data);
                }
            }
        }
        
        return $this->sendResponse($search_result, "$count results found for $request->search"  );

    }

    public function storeBookings(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
               'name'=> 'required',
               'email'=> 'required',
               'phone'=> 'required',
               'desc'=> 'required',
               'destination'=> 'required',
               'bk_date'=> 'required',
               'hour'=> 'required',
               'mins'=> 'required',
               'meridian'=> 'required',
               'pickup' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError($validator->messages(), 1);
            }
            $bookings = new \App\Booking;
            // dd($request->all());
            $bookings->name = $request->name;
            $bookings->email = $request->email;
            $bookings->phone = $request->phone;
            $bookings->desc = $request->desc;
            $bookings->destination = $request->destination;
            $bookings->bk_date = $request->bk_date;
            $bookings->hour = $request->hour;
            $bookings->mins = $request->mins;
            $bookings->meridian = $request->meridian;
            $bookings->pickup = $request->pickup;
            $bookings->save($request->all());
            return $this->sendResponse($bookings, "success"  );
        }catch(\Exception $e) {
            return $this->sendError(env('APP_DEBUG')?$e->getMessage():"Something went wrong", 1);
        }

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurants;
use App\RestaurantsCategory;
use App\Category;

class RestaurantController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function restaurantSubCategory($id) {
    	$category = Category::query()->select(['name'])->where('id', $id)->first();
    	$rest = RestaurantsCategory::where('master_category', $id)->get();
        return view('restaurants-list')->with(['restaurants' => $rest, 'category' => $category]);
    }

    public function restaurantList($master_category, $sub_category) {
    	$category = Category::query()->select(['id','name', 'c_logo'])->where('id', $master_category)->first();
    	$sub_cat = RestaurantsCategory::query()->select(['id','name','c_logo'])->where('id', $sub_category)->first();

    	$rest = Restaurants::where(['master_cat' => $master_category, 'cat_id' => $sub_category])->get();
        if($master_category == 4) {
            return view('celeb_restaurants')->with(['restaurants' => $rest, 'category' => $category, 'sub_category' => $sub_cat]);
        } else {
            if($master_category == 1) {
                return view('rin')->with(['restaurants' => $rest, 'category' => $category, 'sub_category' => $sub_cat]);
            } else if($master_category == 2) {
                return view('standalone')->with(['restaurants' => $rest, 'category' => $category, 'sub_category' => $sub_cat]);
            } else {
                return view('restaurants')->with(['restaurants' => $rest, 'category' => $category, 'sub_category' => $sub_cat]);
            }
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\RestaurantsCategory;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = RestaurantsCategory::all();
        return view('admin.restaurant-category.view_restaurant')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.restaurant-category.add_restaurant')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:restaurant_category|max:255',
                'logo' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'master_category' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $rcat = new RestaurantsCategory;
            if($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $name = md5(time()).'.'.$logo->getClientOriginalExtension();
                $destinationPath = public_path('/images/uploads');
                $logo->move($destinationPath,$name);
                $rcat->logo = 'images/uploads/'.$name;
            }
            if($request->hasFile('c_logo')) {
                $clogo = $request->file('c_logo');
                $cname = md5(time()).'.'.$clogo->getClientOriginalExtension();
                $destinationPath = public_path('/images/uploads');
                $clogo->move($destinationPath,$cname);
                $rcat->c_logo = 'images/uploads/'.$cname;
            }

            $rcat->name = $request->name;
            $rcat->master_category = $request->master_category;          
            /*$rcat->description = 'Demo description';*/
            $rcat->save();
            $message = 'Success fully added a new restaurant category.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->back()->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ;
        // try{
        //     if(is_numeric($id)){
        //         $rcat = RestaurantsCategory::where('id', $id)->first();
        //         if($rcat) {
        //             return $rcat;
        //         }else {
        //             $error['error'] = 'No data found!'; 
        //         }
        //     }else {
        //         $error['error'] = 'Invalid restaurant\'s category id';
        //     }
        // }catch(\Exception $e) {
        //     return $e->getMessage();
        // }
        // return redirect()->back()->with('success', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rcat = RestaurantsCategory::find($id);
        $categories = Category::all();
        return view('admin.restaurant-category.edit_restaurant_category')->with(['res_catgories'=>$rcat,'categories'=>$categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'master_category' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }
            if($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $name = md5(time()).'.'.$logo->getClientOriginalExtension();
                $destinationPath = public_path('/images/uploads');
                $logo->move($destinationPath,$name);
            }
            if($request->hasFile('c_logo')) {
                $clogo = $request->file('c_logo');
                $cname = md5(time()).'.'.$clogo->getClientOriginalExtension();
                $destinationPath = public_path('/images/uploads');
                $clogo->move($destinationPath,$cname);
            }
            $rcat = RestaurantsCategory::find($id);
            $rcat->name = $request->name??$rcat->name;
            $rcat->master_category = $request->master_category??$rcat->master_category;
            if($request->hasFile('logo')) {
                if(file_exists(public_path().'/'.$rcat->logo)) {
                    unlink(public_path().'/'.$rcat->logo);  
                }
                $rcat->logo = 'images/uploads/'.$name;                
            }
            if($request->hasFile('c_logo')) {
                if(!empty($rcat->c_logo)) {
                    if(file_exists(public_path().'/'.$rcat->c_logo)) {
                        unlink(public_path().'/'.$rcat->c_logo);  
                    }
                }
                $rcat->c_logo = 'images/uploads/'.$cname;
            }
            /*$rcat->description = $request->description??$rcat->description;*/
            $rcat->save();
            $message = 'Successfully updated restaurant\'s category.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RestaurantsCategory::destroy($id);
        return redirect()->back()->with('success', 'Successfully deleted restaurant\'s category.');
    }

    public function appendRestCat(Request $request) {
        if($request->ajax()) {
            $disabled = 'disabled';
            $data = $request->all();
            if($data['mas_id'] == 1 || $data['mas_id'] == 2) {
                $disabled = '';
            }
            $cat = RestaurantsCategory::where('master_category', $data['mas_id'])->get();
            return view('admin.restaurant-category.append_rest_cat')->with(['restaurants' => $cat, 'cat_id' => $data['rc'], 'disabled' => $disabled]);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\SuppliersData;
use App\ServicesCategory;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = SuppliersData::all();
        return view('admin.supplier.view_category')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = ServicesCategory::all();
        return view('admin.supplier.add_category')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:category|max:255',
                'parent_cat' => 'required',
                'desc' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'profile_image' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'mobile' => 'required',
                'email' => 'required|email',
                'website_url' => 'required|max:255',
                'redirect_url' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $image = $this->uploadImage('image', $request);
            $logo = $this->uploadImage('profile_image', $request);

            $category = new SuppliersData;
            $category->name = $request->name??"";
            $category->parent_cat = $request->parent_cat??"";
            $category->desc = $request->desc??"";
            $category->mobile = $request->mobile??"";
            $category->alt_mobile = $request->alt_mobile??"";
            $category->email = $request->email??"";
            $category->alt_email = $request->alt_email??"";
            $category->website_url = $request->website_url??"";
            $category->redirect_url = $request->redirect_url??"";
            
            $category->image = !empty($image)?'images/uploads/'.$image:'';
            $category->profile_image = !empty($logo)?'images/uploads/'.$logo:'';
            
            $category->save();
            $message = 'Success fully added a new supplier.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return back()->with('success',$message);
    }

    public function uploadImage($image_name,Request $request)
    {
        if($request->hasFile($image_name)) {
            $image = $request->file($image_name);
            $name = md5(time().$image_name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/uploads');
            $image->move($destinationPath,$name);
            return $name;
        }else {
            return false;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // try{
        //     if(is_numeric($id)){
        //         $category = ServicesCategory::where('id', $id)->first();
        //         if($category) {
        //             return $category;
        //         }else {
        //             $error['error'] = 'No data found!'; 
        //         }
        //     }else {
        //         $error['error'] = 'Invalid category id';
        //     }
        // }catch(\Exception $e) {
        //     return $e->getMessage();
        // }
        // return redirect('category')->with('success', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = SuppliersData::find($id);
        $category = ServicesCategory::all();
        return view('admin.supplier.edit_category')->with(['categories'=>$category,'supplier'=>$supplier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:category|max:255',
                'parent_cat' => 'required',
                'desc' => 'required',
                'mobile' => 'required',
                'email' => 'required|email',
                'website_url' => 'required|max:255',
                'redirect_url' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $image = $this->uploadImage('image', $request);
            $logo = $this->uploadImage('profile_image', $request);

            $supplier = SuppliersData::find($id);
            $supplier->name = $request->name??$supplier->name;
            $supplier->parent_cat = $request->parent_cat??$supplier->parent_cat;
            $supplier->desc = $request->desc??$supplier->desc;
            $supplier->mobile = $request->mobile??$supplier->mobile;
            $supplier->alt_mobile = $request->alt_mobile??$supplier->alt_mobile;
            $supplier->email = $request->email??$supplier->email;
            $supplier->alt_email = $request->alt_email??$supplier->alt_email;
            $supplier->website_url = $request->website_url??$supplier->website_url;
            $supplier->redirect_url = $request->redirect_url??$supplier->redirect_url;
            
            $supplier->image = !empty($image)?'images/uploads/'.$image:$supplier->image;
            $supplier->profile_image = !empty($logo)?'images/uploads/'.$logo:$supplier->profile_image;
            
            $supplier->save();
            $message = 'Successfully updated supplier.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SuppliersData::destroy($id);
        return redirect()->back()->with('success', 'Successfully deleted supplier.');
    }
}

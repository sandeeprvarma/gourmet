<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ServicesCategory;

class SupplierCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ServicesCategory::all();
        return view('admin.supplier_category.view_category')->with('categories',$categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.supplier_category.add_category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:category|max:255',
                'logo' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            if($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $name = md5(time()).'.'.$logo->getClientOriginalExtension();
                $destinationPath = public_path('/images/uploads');
                $logo->move($destinationPath,$name);
                // $logo->save();
            }

            $category = new ServicesCategory;
            $category->name = $request->name;
            $category->logo = 'images/uploads/'.$name;
            $category->save();
            $message = 'Success fully added a new category.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return back()->with('success',$message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // try{
        //     if(is_numeric($id)){
        //         $category = ServicesCategory::where('id', $id)->first();
        //         if($category) {
        //             return $category;
        //         }else {
        //             $error['error'] = 'No data found!'; 
        //         }
        //     }else {
        //         $error['error'] = 'Invalid category id';
        //     }
        // }catch(\Exception $e) {
        //     return $e->getMessage();
        // }
        // return redirect('category')->with('success', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ServicesCategory::find($id);
        return view('admin.supplier_category.edit_category')->with('category',$category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return back()
                    ->withErrors($validator)
                    ->withInput();
        }

        try {
            $category = ServicesCategory::find($id);
            if($request->hasFile('logo')) {
                $logo = $request->file('logo');
                $name = md5(time()).'.'.$logo->getClientOriginalExtension();
                $destinationPath = public_path('/images/uploads');
                $logo->move($destinationPath,$name);
                if(file_exists(public_path().'/'.$category->logo)) {
                    unlink(public_path().'/'.$category->logo);
                }
            }

            if($category->name != $request->name){
                $category->name = $request->name;
            }
            if(!empty($request->logo)) {
                $category->logo = 'images/uploads/'.$name;
            }
            $category->save();
            $message = 'Successfully updated category.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServicesCategory::destroy($id);
        return redirect()->back()->with('success', 'Successfully deleted category.');
    }
}

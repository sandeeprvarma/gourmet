<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\RestaurantsCategory;
use App\Restaurants;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RestaurantsController extends Controller
{
    /**
     * Display a listing $image_namethe resource.
  $image_name     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurants::all();
        return view('admin.restaurants.view_restaurant',['restaurants'=>$restaurants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hide ='display:none;'; 

        $main_categories = Category::all();
        // $main_categories = $main_categories->toArray();
        /*$restaurants = RestaurantsCategory::all();*/
        $restaurants = array();
       
        // $categories = $main_categories->merge($restaurants);
        return view('admin.restaurants.add_restaurant',['categories'=>$main_categories,'restaurants'=>$restaurants,'hide'=>$hide, 'disabled' => 'disabled']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:restaurant|max:255',
                'master_cat' => 'required',
                'image' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $image = $this->uploadImage('image', $request);
            $logo = $this->uploadImage('logo', $request);
            $celeb = $this->uploadImage('celeb_image', $request);

            $restaurant = new Restaurants;
            $restaurant->name = $request->name;
            $restaurant->master_cat = $request->master_cat;
            $restaurant->cat_id = $request->cat_id??0;
            $restaurant->image = 'images/uploads/'.$image;
            $restaurant->logo = !empty($logo)?'images/uploads/'.$logo:'';
            $restaurant->celeb_image = !empty($celeb)?'images/uploads/'.$celeb:'';

            $restaurant->description = $request->description;
            $restaurant->site_url = $request->site_url;
            $restaurant->save();
            $message = 'Success fully added a new restaurant.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return redirect()->back()->with('success',$message);
    }

    public function uploadImage($image_name,Request $request)
    {
        if($request->hasFile($image_name)) {
            $image = $request->file($image_name);
            $name = md5(time().$image_name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/uploads');
            $image->move($destinationPath,$name);
            return $name;
        }else {
            return false;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function show(restaurants $restaurants)
    {
        try{
            if(is_numeric($id)){
                $restaurants = Restaurants::where('id', $id)->first();
                if($restaurants) {
                    return $restaurants;
                }else {
                    $error['error'] = 'No data found!'; 
                }
            }else {
                $error['error'] = 'Invalid restaurants id';
            }
        }catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $disabled = 'disabled';
        $hide ='display:none;'; 
        $restaurant = Restaurants::find($id);
        $restaurants = RestaurantsCategory::all();
        $main_categories = Category::all();
        if($restaurant->master_cat == 1 || $restaurant->master_cat == 2) {
           $disabled = '';
        }

        if($restaurant->master_cat == 4 || $restaurant->master_cat == 3) {
           $hide = '';
        }
        return view('admin.restaurants.edit_restaurant')->with(['categories'=>$main_categories,'restaurant'=>$restaurant,'restaurants'=>$restaurants,'disabled'=>$disabled,'hide'=>$hide]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'master_cat' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                            ->withErrors($validator)
                            ->withInput();
            }

            $image = $this->uploadImage('image', $request);
            $logo = $this->uploadImage('logo', $request);
            $celeb = $this->uploadImage('celeb_image', $request);

            $cate_id = 0;
            if(!empty($request->cat_id)) {
                $cate_id = $request->cat_id;
            }

            $restaurant = Restaurants::find($id);
            $restaurant->name = $request->name??$restaurant->name;
            $restaurant->master_cat = $request->master_cat??$restaurant->master_cat;
            /*$restaurant->cat_id = $request->cat_id??$restaurant->cat_id;*/
            $restaurant->cat_id = $cate_id;
            $restaurant->image = $image?'images/uploads/'.$image:$restaurant->image;
            $restaurant->logo = !empty($logo)?'images/uploads/'.$logo:$restaurant->logo;
            $restaurant->celeb_image = !empty($celeb)?'images/uploads/'.$celeb:$restaurant->celeb_image;

            $restaurant->description = $request->description??$restaurant->description;
            $restaurant->site_url = $request->site_url??$restaurant->site_url;

            $restaurant->save();
            $message = 'Successfully updated restaurant.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\restaurants  $restaurants
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Restaurants::destroy($id);
        return redirect()->back()->with('success', 'Successfully deleted restaurant.');
    }
}

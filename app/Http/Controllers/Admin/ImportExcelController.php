<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelController extends Controller
{
    private $model = array('category' => 'Category',
                    'restaurant' => 'Restaurants',
                    'restaurant_category' => 'RestaurantsCategory',
                    'services_category' => 'ServicesCategory',
                    'suppliers_data' => 'SuppliersData'
                );
    public function importForm($table)
    {
        return view('admin.import')->with(['table'=>$table,'model'=>$this->model]);
    }
    
    public function import(Request $request,$table) 
    {
        try {
            $this->validate($request, [
                'excel_file'  => 'required|mimes:xlsx,xls'
            ]);
            $class = "App\\Imports\\". $this->model[$table] . "Import";
            $import = Excel::import(new $class,request()->file('excel_file'));
            return back()->with('success','Successfully imported data');
        }catch(\Maatwebsite\Excel\Validators\ValidationException $e) {
            return back()->withError($e->failures());
        }
    }

    public function uploadImages(Request $request)
    {
        $request->validate([
          'image' => 'required'
        ]);
        if ($request->hasFile('image')) {
            foreach($request->file('image') as $file){
                $filePath = $this->UserImageUpload($file);
            }
        }
        return redirect()->back()->with('success','Images uploaded successfully');
    }

    public function UserImageUpload($query) // Taking input image as parameter
    {
        $image_name = $query->getClientOriginalName();
        $ext = strtolower($query->getClientOriginalExtension()); // You can use also getClientOriginalName()
        $image_full_name = $image_name.'.'.$ext;
        $upload_path = public_path('/images/uploads');    //Creating Sub directory in Public folder to put image
        $image_url = $upload_path.$image_full_name;
        $success = $query->move($upload_path,$image_full_name);
        return $image_url; // Just return image
    }
}
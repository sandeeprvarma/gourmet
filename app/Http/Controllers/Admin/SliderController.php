<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$sliders = Slider::all();*/
        //$sliders = Slider::orderBy('id','asc')->get();
        $sliders = Slider::orderBy('slider_type')->get();
        return view('admin.slider.view_slider')->with('sliders',$sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.add_slider');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'desc' => 'required|max:255',
                'image' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'rank' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $image = $this->uploadImage('image', $request);

            $slide = new Slider;
            $slide->desc = $request->desc;
            $slide->rank = $request->rank;
            $slide->slider_type = $request->slider_type;            
            $slide->image = !empty($image)?'images/uploads/'.$image:'';
            
            $slide->save();
            $message = 'Slider added Successfully.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }

        return back()->with('success',$message);
    }

    public function uploadImage($image_name,Request $request)
    {
        if($request->hasFile($image_name)) {
            $image = $request->file($image_name);
            $name = md5(time().$image_name).'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/uploads');
            $image->move($destinationPath,$name);
            return $name;
        }else {
            return false;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('admin.slider.edit_slider')->with('slider',$slider);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'desc' => 'required|max:255',
                'image' => 'required|mimes:jpeg,jpg,png,gif,svg|max:2048',
                'rank' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }

            $image = $this->uploadImage('image', $request);

            $slide = Slider::find($id);
            $slide->desc = $request->desc??$slide->desc;
            $slide->rank = $request->rank??$slide->rank;
            $slide->slider_type = $request->slider_type??$slide->slider_type;            
            $slide->image = !empty($image)?'images/uploads/'.$image:$slide->image;
            
            $slide->save();
            $message = 'Successfully updated slider.';
        }catch(\Exception $e) {
            return $e->getMessage();
        }
        return redirect()->back()->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::destroy($id);
        return redirect()->back()->with('success', 'Successfully deleted slider.');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Booking;

class BookingsController extends Controller
{
    public function index()
    {
    	$bookings = Booking::all();
    	return view('admin.bookings')->with('bookings',$bookings);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurants;
use App\Category;
use App\RestaurantsCategory;
use App\Slider;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $sliders = Slider::where('slider_type',1)->orderBy('rank')->get();
        return view('home')->with(['categories' => $categories, 'sliders' => $sliders]);
    }

    public function aboutUs()
    {
        $sliders = Slider::where('slider_type',2)->orderBy('rank')->get();
        return view('aboutus')->with('sliders' , $sliders);
    }

    public function booking()
    {
        return view('booking');
    }

    public function storeBookings(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
               'name'=> 'required',
               'email'=> 'required',
               'phone'=> 'required',
               'desc'=> 'required',
               'destination'=> 'required',
               'bk_date'=> 'required',
               'hour'=> 'required',
               'mins'=> 'required',
               'meridian'=> 'required',
               'pickup' => 'required'
            ]);

            if ($validator->fails()) {
                return back()
                        ->withErrors($validator)
                        ->withInput();
            }
            $bookings = new \App\Booking;
            // dd($request->all());
            $bookings->name = $request->name;
            $bookings->email = $request->email;
            $bookings->phone = $request->phone;
            $bookings->desc = $request->desc;
            $bookings->destination = $request->destination;
            $bookings->bk_date = $request->bk_date;
            $bookings->hour = $request->hour;
            $bookings->mins = $request->mins;
            $bookings->meridian = $request->meridian;
            $bookings->pickup = $request->pickup;
            $bookings->save($request->all());
            return back()->with('success','Booking successfull');
        }catch(\Exception $e) {
            throw new \Exception(env('APP_DEBUG')?$e->getMessage():"Something went wrong", 1);
            
        }

    }

    public function searchResult(Request $request) {
        $search_result = array();
        $count = 0;
        $category_data = Category::where('name', 'LIKE', "%{$request->search_val}%")->get();
        if(!$category_data->isEmpty()) {
            foreach ($category_data as $key => $category) {
                if($category->id == 1) {
                    $sub_cat = RestaurantsCategory::all();
                    foreach ($sub_cat as $skey => $sc) {
                        $restaurant_data = Restaurants::query()->select(['id','name', 'image', 'description', 'site_url','master_cat','cat_id'])->where(['master_cat' => $category->id, 'cat_id' => $sc->id])->get();
                        if(!$restaurant_data->isEmpty()) {
                            $count += count($restaurant_data);
                            array_push($search_result, $restaurant_data);
                        }
                    }
                } else {
                    $restaurant_data = Restaurants::query()->select(['id','name', 'image', 'description', 'site_url','master_cat','cat_id'])->where('master_cat',$category->id)->get();
                    if(!$restaurant_data->isEmpty()) {
                        $count += count($restaurant_data);
                        array_push($search_result, $restaurant_data);
                    }
                }
            }
        }
        $restaurant = Restaurants::where('name', 'LIKE', "%{$request->search_val}%")->get();
        if(!$restaurant->isEmpty()) {
            $count += count($restaurant);
            array_push($search_result, $restaurant);
        }

        $restaurant_cat = RestaurantsCategory::where('name', 'LIKE', "%{$request->search_val}%")->get();
        if(!$restaurant_cat->isEmpty()) {
            foreach ($restaurant_cat as $skey => $sc) {
                $restaurant_data = Restaurants::query()->select(['id','name', 'image', 'description', 'site_url','master_cat','cat_id'])->where(['master_cat' => $category->id, 'cat_id' => $sc->id])->get();
                if(!$restaurant_data->isEmpty()) {
                    $count += count($restaurant_data);
                    array_push($search_result, $restaurant_data);
                }
            }
        }
        
        return view('search')->with(['search_result' => $search_result, 'count' => $count, 'search_string' => $request->search_val]);
    }
}

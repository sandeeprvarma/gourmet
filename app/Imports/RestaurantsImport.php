<?php

namespace App\Imports;

use App\Restaurants;
use App\RestaurantsCategory;
use App\Category;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RestaurantsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $mastercat = $retcat = [];
        
        if(!empty($row['name'])) {
            try {
                if(!empty($row['cat_id'])) {
                    $retcat = RestaurantsCategory::where('name','=' ,$row['cat_id'])->first();
                    if(empty($retcat)) {
                        dd('Restaurants Category '.$row['cat_id'].' does not exist');
                    }
                }
                if(!empty($row['master_cat'])) {
                    $mastercat = Category::where('name','=' ,$row['master_cat'])->first();
                    if(empty($mastercat)) {
                        dd('Master Category '.$row['master_cat'].' does not exist');
                    }
                }

                return new Restaurants([
                    'name' => $row['name'],
                    'cat_id' => $retcat->id??0,
                    'image' => $row['image']?'images/uploads/'.$row['image']:'',
                    'description' => $row['description'],
                    'site_url' => $row['site_url']??'',
                    'logo' => $row['logo']?'images/uploads/'.$row['logo']:'',
                    'celeb_image' => $row['celeb_image']?'images/uploads/'.$row['celeb_image']:'',
                    'master_cat' => $mastercat->id
                ]);
            }catch(\Exception $e) {
                dd($e->getMessage().json_encode($row));
            }
        }
    }
}

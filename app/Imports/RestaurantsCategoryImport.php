<?php

namespace App\Imports;

use App\RestaurantsCategory;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Category;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class RestaurantsCategoryImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!empty($row['name'])) {
            try {
                if(!empty($row['master_cat'])) {
                    $mastercat = Category::where('name','=' ,$row['master_cat'])->first();
                    if(empty($mastercat)) {
                        dd('Master Category does not exist');
                    }
                }
                return new RestaurantsCategory([
                    'name' => $row['name'],
                    'master_category' => $mastercat->id,
                    'logo' => $row['logo'],
                    'description'  => $row['description'],
                ]);
            }catch(\Exception $e) {
                // dd($e->getMessage().json_encode($row));
            }
        }
    }


    public function chunkSize(): int
    {
        return 1000;
    }
}

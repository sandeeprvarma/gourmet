<?php

namespace App\Imports;

use App\Category;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CategoryImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!empty($row['name'])) {
            try {
                return new Category([
                    'name' => $row['name'],
                    'logo' => $row['logo'],
                ]);
            }catch(\Exception $e) {
                // dd($e->getMessage().json_encode($row));
            }
        }
    }
}

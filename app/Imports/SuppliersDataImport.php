<?php

namespace App\Imports;

use App\SuppliersData;
use App\ServicesCategory;
use Maatwebsite\Excel\Concerns\ToModel;

class SuppliersDataImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!empty($row['name'])) {
            if(!empty($row['parent_category'])) {
                $mastercat = ServicesCategory::where('name','=' ,$row['parent_category'])->first();
                if(empty($mastercat)) {
                    dd('Master Category does not exist');
                }
            }
            return new SuppliersData([
                'name'=> $row['name'],
                'parent_cat'=> $mastercat->id,
                'desc'=> $row['desc'],
                'image'=> $row['image'],
                'profile_image'=> $row['profile_image'],
                'mobile'=> $row['mobile'],
                'alt_mobile'=> $row['alt_mobile'],
                'email'=> $row['email'],
                'alt_email'=> $row['alt_email'],
                'website_url'=> $row['website_url'],
                'redirect_url'=> $row['redirect_url'],
            ]);
        }
    }
}

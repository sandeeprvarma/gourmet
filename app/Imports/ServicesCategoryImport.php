<?php

namespace App\Imports;

use App\ServicesCategory;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ServicesCategoryImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!empty($row['name'])) {
            return new ServicesCategory([
                'name' => $row['name'],
                'logo' => $row['logo'],
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}

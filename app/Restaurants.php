<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
    protected $table = 'restaurant';

    protected $fillable = ['name','cat_id','image','description','site_url','logo','celeb_image','master_cat'];
}

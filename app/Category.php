<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	/***
	* Table name
	*/
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'logo', 'link_url', 'c_logo'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppliersData extends Model
{
    protected $table = 'suppliers_data';
    protected $fillable = ['name', 'parent_cat', 'desc', 'image', 'profile_image', 'mobile','alt_mobile','email', 'alt_email','website_url','redirect_url'];
}

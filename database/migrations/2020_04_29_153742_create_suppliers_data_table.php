<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers_data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('parent_cat');
            $table->text('desc');
            $table->text('image');
            $table->text('profile_image');
            $table->string('mobile');
            $table->string('alt_mobile');
            $table->string('email');
            $table->string('alt_email');
            $table->string('website_url');
            $table->string('redirect_url');
            $table->timestamps();
            //Suppliers: id, name, parent_cat, desc, image,mobile,alt_mobile,email, alt_email,website_url,redirect_url
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers_data');
    }
}

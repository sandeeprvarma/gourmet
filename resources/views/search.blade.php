@extends('layouts.app')
@section('content')
<section id="about-us" class="about-us">
    <div class="container">
    	<div class="row">
    		<div class="col-md-12 col-sm-6 result_count">
    			{{$count}} results found for "{{$search_string}}"
    		</div>
    	</div>
    	@if(!empty($count))
	    	<div class="row mt-4">
	    		<div class="col-md-12 col-sm-6 result_text">
	    			Your search result
	    		</div>
	    	</div>
	    	@foreach($search_result as $result)
	    		@foreach($result as $res)
		    	<div class="row search_container">
		    		<div class="col-md-4 col-sm-2" style="padding-left: 0;">
		    			<img src="{{url($res->image)}}" class="img-fluid">
		    		</div>
		    		<div class="col-md-6 col-sm-2 align-self-center search_desc">
		    			<p class="text-uppercase">
		    				{{$res->name}}
		    			</p>
		    			<p>
		    				{{$res->description}}
		    			</p>
		    		</div>
		    		<div class="col-md-2 col-sm-2 align-self-center">
		    			<a class="show_more" href="{{url('restaurant/'.$res->master_cat.'/'.$res->cat_id)}}" tabindex="-1">show more</a>
		    		</div>
		    	</div>
		    	@endforeach
	    	@endforeach
    	@endif

    	<!-- <div class="row search_container">
    		<div class="col-md-4 col-sm-2">
    			<img src="http://localhost:8000/images/final-logo.png" class="img-fluid">
    		</div>
    		<div class="col-md-6 col-sm-2 align-self-center search_desc">
    			<p class="text-uppercase">
    				Flighst of fancy
    			</p>
    			<p>
    				Enjoy the soaring feeling of the best of in-flight fine-dining at 30,000 feet while being on ground zero - and all that without worrying about being late for boarding.
    			</p>
    		</div>
    		<div class="col-md-2 col-sm-2 align-self-center">
    			<a class="read_more" href="/booking" tabindex="-1">enquiry</a>
    		</div>
    	</div> -->
    </div>
</section>
@endsection
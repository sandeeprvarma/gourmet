@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<div class="notfound">
			  <h1>404</h1>
			  <h2>OOPS! SOME ERROR OCCURED!</h2>
			  <a href="/">Go TO Homepage</a>
			</div>
		</div>
	</div>
</div>
@extends('layouts.app')
@section('content')
@if(!$sliders->isEmpty())
<section id="hero">
    <div class="hero-container">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
            <!-- <ol class="carousel-indicators" id="hero-carousel-indicators"></ol> -->
            <div class="carousel-inner" role="listbox">
                @foreach($sliders as $key => $slider)
                <div class="carousel-item {{$key == 0? 'active':''}}">
                    <img src="{{url($slider->image)}}" class="img-fluid car_img">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <!-- <h2 class="animated fadeInDown">{{$slider->desc}}</h2> -->
                            <div class="ml-auto">
                                <a href="/about" class="btn-menu animated fadeIn" style="font-family: 'Gotham Book', 'Poppins', sans-serif;">WHAT WE DO FOR YOU</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
@endif
<section class="ftco-section">
  <div class="container">
    <div class="row justify-content-center">
      @if(!$categories->isEmpty())
        <div style="width: 85%;">
          <div class="row">
        @foreach($categories as $category)
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
            <div class="card text-center text-white mb-4">
              <img class="card-img" src="{{ url($category->logo) }}" alt="post-thumb">
              <div class="card-img-overlay">
                <div class="card-content rest_details">
                  <h4 class="card-title mb-4 text-white text-uppercase">{{ $category->name }}</h4>
                  @if($category->id == 1 || $category->id == 2)
                  <a class="see_more" href="{{ route('rest_subcat',$category->id)}}" tabindex="-1">see more</a>
                  @else
                  <a class="see_more" href="{{ route('restaurant',[$category->id, 0]) }}" tabindex="-1">see more</a>
                  @endif
                </div>
              </div>
            </div>
          </div>
        @endforeach
          </div>
        </div>
      @else
        <div class="col-lg-12 text-center empty_result">
            <div class="face">
              <div class="band">
                <div class="red"></div>
                <div class="white"></div>
                <div class="blue"></div>
              </div>
              <div class="eyes"></div>
              <div class="dimples"></div>
              <div class="mouth"></div>
            </div>
            <h1>Oops! Nothing Found Here!</h1>
            <a href="/">Go TO Homepage</a>
          </div>
      @endif
    </div>
  </div>
</section>
@endsection
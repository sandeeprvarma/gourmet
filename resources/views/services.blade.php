@extends('layouts.app')
@section('content')
@if(!$sliders->isEmpty())
<section id="hero">
    <div class="hero-container">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
            <!-- <ol class="carousel-indicators" id="hero-carousel-indicators"></ol> -->
            <div class="carousel-inner" role="listbox">
                @foreach($sliders as $key => $slider)
                <div class="carousel-item {{$key == 0? 'active':''}}">
                    <img src="{{url($slider->image)}}" class="img-fluid car_img">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <!-- <h2 class="animated fadeInDown">{{$slider->desc}}</h2> -->
                            <div class="ml-auto">
                                <a href="/about" class="btn-menu animated fadeIn" style="font-family: 'Gotham Book', 'Poppins', sans-serif;">WHAT WE DO FOR YOU</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
@endif
<section id="suppliers" class="suppliers-info">
	<div class="container">
	    <div class="row">
	    	@if(!$services->isEmpty())
		    	@foreach($services as $service)
		    	<div class="col-lg-6 col-sm-6">
			        <div class="card text-center back-color img_container rounded-0 mb-4">
			        	<img class="card-img img-fluid rounded-0" style="border: 1px solid #fff;" src="{{url($service->logo)}}" alt="post-thumb">
			          	<div class="card-footer ser_name text-center back-color text-uppercase"><a href="{{ route('service_providers',$service->id) }}">{{ $service->name }}</a></div>
			        </div>
				</div>
				@endforeach
			@else
				<div class="col-lg-12 text-center empty_result">
			        <div class="face">
                        <div class="band">
                            <div class="red"></div>
                            <div class="white"></div>
                            <div class="blue"></div>
                        </div>
                        <div class="eyes"></div>
                        <div class="dimples"></div>
                        <div class="mouth"></div>
                    </div>
                    <h1>Oops! No Results found!</h1>
                    <a href="/">Go TO Homepage</a>
			     </div>
			@endif
	    </div>
	</div>
</section>
@endsection
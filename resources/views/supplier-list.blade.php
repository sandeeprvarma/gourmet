@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row mt-2">
		<div class="col-lg-12">
	        <nav aria-label="breadcrumb">
	        	<ol class="breadcrumb">
	            	<li class="breadcrumb-item"><a class="black-text" href="/services">Supplier</a></li>
	            	<li class="breadcrumb-item active">{{$services->name}}</li>
	          </ol>
	        </nav>
		</div>
    </div>
</div>
<section class="suppliers-list">
	<div class="container">
		@if(!$suppliers_list->isEmpty())
			@foreach($suppliers_list as $skey => $supplier)
				@if ($skey % 2 == 0)
			        <div class="row mb-5">
			            <div class="col-lg-6 col-sm-6" style="background-color: #000;">
			                <div class="img_holder pt-3">
			                    <div class="main_img_holder">
			                        <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                    </div>
			                    <!-- <div class="thumbs">
			                        <div>
			                            <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                        </div>
			                        <div>
			                            <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                        </div>
			                        <div>
			                            <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                        </div>
			                    </div> -->
			                </div>
			                <div class="text-center mt-3 supplier text-uppercase">
			                    <p>{{ $supplier->name }}</p>
			                </div>
			            </div>
			            <div class="col-md-6 borders-right">
			                <div class="image-wrapper float-left pt-4 pl-2 pr-3">
			                    <img class="img-fluid" src="{{url($supplier->profile_image)}}" style="width: 150px;height: 200px;" alt="">
			                </div>
			                <div class="sup-content-wrapper pl-3 pt-4">
			                	<!-- {{ $supplier->desc }} -->
			                	{!!html_entity_decode($supplier->desc)!!}
			                    <p class="sup_details" style="margin-bottom: 0 !important;">
			                    	<b>T: </b>{{$supplier->mobile}} @if(!empty($supplier->alt_mobile)) <span>;&nbsp;</span>{{$supplier->alt_mobile}} @endif
									<!-- @foreach(explode(',', $supplier->contact) as $contact_info) 
										{{ $contact_info }}
									@endforeach -->
			                    </p>
			                    <p class="sup_details">
			                    	<b>E: </b>{{$supplier->email}} @if(!empty($supplier->alt_email)) <span>;&nbsp;</span>{{$supplier->alt_email}} @endif
			                    </p>
			                    <p class="supp_link">for more details, click on <a href="{{ route('supplier_info',$supplier->id)}}">{{$supplier->website_url}}</a></p>
			                </div>
			            </div>
			        </div>
			    @else
				    <div class="row mb-5">
			            <div class="col-md-6 borders-left">
			                <div class="image-wrapper float-left pt-4 pl-2 pr-3">
			                    <img class="img-fluid" src="{{url($supplier->profile_image)}}" style="width: 150px;height: 200px;" alt="">
			                </div>
			                <div class="sup-content-wrapper pl-3 pt-4">
			                    <!-- {{$supplier->desc}} -->
			                    {!!html_entity_decode($supplier->desc)!!}
			                    <p class="sup_details" style="margin-bottom: 0 !important;">
			                    	<b>T: </b>{{$supplier->mobile}} @if(!empty($supplier->alt_mobile)) <span>;&nbsp;</span>{{$supplier->alt_mobile}} @endif
			                    </p>
			                    <p class="sup_details">
			                    	<b>E: </b>{{$supplier->email}} @if(!empty($supplier->alt_email)) <span>;&nbsp;</span>{{$supplier->alt_email}} @endif
			                    </p>
			                    <p class="supp_link">for more details, click on <a href="{{ route('supplier_info',$supplier->id)}}">{{$supplier->website_url}}</a></p>
			                </div>
			            </div>
			            <div class="col-lg-6 col-sm-6" style="background-color: #000;">
			                <div class="img_holder pt-3">
			                    <div class="main_img_holder">
			                        <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                    </div>
			                    <!-- <div class="thumbs">
			                        <div>
			                            <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                        </div>
			                        <div>
			                            <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                        </div>
			                        <div>
			                            <img class="card-img img-fluid rounded-0" src="{{url($supplier->image)}}" alt="post-thumb">
			                        </div>
			                    </div> -->
			                </div>
			                <div class="text-center mt-3 supplier text-uppercase">
			                    <p>{{ $supplier->name }}</p>
			                </div>
			            </div>
			        </div>
				@endif
	        @endforeach
	    @else
	    	<div class="col-lg-12 text-center empty_result">
		        <div class="face">
                    <div class="band">
                        <div class="red"></div>
                        <div class="white"></div>
                        <div class="blue"></div>
                    </div>
                    <div class="eyes"></div>
                    <div class="dimples"></div>
                    <div class="mouth"></div>
                </div>
                <h1>Oops! No Results found!</h1>
                <a href="/">Go TO Homepage</a>
		    </div>
	    @endif
	</div>
</section>

@endsection
@extends('layouts.app')
@section('content')
<!-- ======= Book A Table Section ======= -->
<section id="book-a-table" class="book-a-table">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
    </div>
    @endif
    <div class="container">
        <div class="row justify-content-center">
            <div class="bk_holder">           
            
        <form action="{{url('/booking')}}" method="POST" role="form" id="booking_form" class="php-email-form">
            @csrf
            <div class="form-row">
                <div class="col-lg-6 col-md-6 form-group">
                    <label for="name">NAME</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter your name">
                </div>
                <div class="col-lg-6 col-md-6 form-group">
                    <label for="email">EMAIL</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
                </div>
                <div class="col-lg-12 col-md-12 form-group">
                    <label for="phone">PHONE</label>
                    <input type="text" class="form-control" pattern="[0-9]" name="phone" id="phone" placeholder="Enter your phone number">
                </div>
                <div class="col-lg-12 col-md-12 form-group">
                    <label for="desc">DESCRIPTION</label>
                    <input type="text" name="desc" class="form-control" id="desc" placeholder="ie. Restaurant at Home/Experiential Celebration">
                </div>
                <div class="col-lg-12 col-md-12 form-group">
                    <label for="destination">DESTINATION</label>
                    <input type="text" class="form-control" name="destination" id="destination" placeholder="Enter your full address">
                </div>

                <div class="col-lg-8 col-md-8 form-group">
                    <label for="destination">DATE</label>
                    <input type="date" class="form-control" name="bk_date" id="date" placeholder="mm/dd/yyyy">
                </div>

                <input type="hidden" name="hour" value="1">
                <input type="hidden" name="mins" value="00">

                <!-- <div class="col-lg-2 col-md-2 form-group">
                    <label for="hour">HOUR</label>
                    <select class="form-control" name="hour" id="hour">
                        <option value="">SELECT HOUR</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                 </div> -->

                 <!-- <div class="col-lg-2 col-md-2 form-group">
                    <label for="minutes">MIN</label>
                    <select class="form-control" name="mins" id="minutes">
                        <option value="">SELECT MINUTES</option>
                        <option value="00">00</option>
                        <option value="15">15</option>
                        <option value="30">30</option>
                        <option value="45">45</option>
                    </select>
                 </div> -->

                 <div class="col-lg-4 col-md-4 form-group">
                    <label for="meridian">AM/PM</label>
                    <select class="form-control" name="meridian" id="meridian" style="height: 44px;">
                        <option value="">AM/PM</option>
                        <option value="AM">AM</option>
                        <option value="PM">PM</option>
                    </select>
                 </div>                

                <div class="col-lg-12 col-md-12 form-group">
                    <label for="pickup">APPROX. NUMBERS OF GUESTS</label>
                    <input type="text" class="form-control" name="pickup" id="pickup" placeholder="Enter approx. number of attendees">
                </div>
            </div>
            <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your booking request was sent. We will call back or send an Email to confirm your reservation. Thank you!</div>
            </div>
            <button type="submit" class="btn btn-block mb-3">SUBMIT BOOKING FORM</button>
            <p class="text-uppercase" style="margin-bottom: 0;color: #fff;">You will get a return within 24 hrs of submitting form to discuss further details</p>
            <!-- <div class="text-center"><button type="submit">Submit</button></div> -->
        </form>
                </div>
        </div>

    </div>
</section>
<!-- End Book A Table Section -->
@endsection
@extends('layouts.app')
@section('content')
@if(!$sliders->isEmpty())
<section id="hero">
    <div class="hero-container">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
            <!-- <ol class="carousel-indicators" id="hero-carousel-indicators"></ol> -->
            <div class="carousel-inner" role="listbox">
                @foreach($sliders as $key => $slider)
                <div class="carousel-item {{$key == 0? 'active':''}}">
                    <img src="{{url($slider->image)}}" class="img-fluid car_img">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <!-- <h2 class="animated fadeInDown">{{$slider->desc}}</h2> -->
                            <!-- <div class="ml-auto">
                                <a href="/about" class="btn-menu animated fadeIn" style="font-family: 'Gotham Book', 'Poppins', sans-serif;">WHAT WE DO FOR YOU</a>
                            </div> -->
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
@endif
<section id="about-us" class="about-us">
    <div class="container">

        <div class="row justify-content-center mb-4">
          <div class="col-md-12 col-sm-6 about_us_holder">
            <div class="section-title text-center">
                <h2 class="text-uppercase">About <span>Gourmet@Home</span></h2>
                <!-- <h2 class="text-uppercase">About Gourmet@Home</h2> -->
            </div>
            <h4 class="text-center mb-4" style="font-weight: 600;">Bring Dining Out experience In</h4>
            <p>
              Life is a celebration. And to make sure celebrations do not stop, we bring the luxury of <b>“5-star dining experience”</b> to the comfort – and safety – of your home.
            </p>
            <p>
              Not just signature dishes from the fine-dine restaurant of your choice in India or abroad, we recreate the entire dining out experience as a state-of-the-art avant-garde event. From the look and feel to the napkins, tableware, table, chair, wall décor, flooring, music and, yes, even the view, we bring it all together – with the chef and select team member(s) in tow.
            </p>
            <p>
              We work with the biggest and most respected five-star hotel and standalone restaurant brands with unmatched reputation for maintaining the highest safety and hygiene standards and ensuring minimum human contact. 
            </p>
            <p>
              But that’s not all. We go a step further and also recreate any other novel gastronomic experience that you wish!
            </p>

            <p>
              So, whether it is a <b>reception, anniversary, date night, birthday, festive occasion</b> or an <b>intimate wedding celebration</b>, all you need to do is go to the “Booking” section and let us know your requirements and allow us to turn your dreams into reality by creating a customised dining ‘experience’ like no other!
            </p>

            <p>
              After the simultaneous launch of the exclusive all-new fine dine in concept in Mumbai and New Delhi we will, over time, introduce the service in other metros.
            </p>
            <p class="font-italic" style="font-family: 'Chronicle Display Italic';font-size: 21px;">
              * This exclusive fine-dining experience at home comes for a price tag and a minimum guarantee.
            </p>
          </div>
        </div>

        <!-- <div class="row section-title">
          <div class="col-md-12 col-sm-6">
            <h5 class="text-uppercase text-center">ABOUT <span>TRIKAYA MEDIA GROUP</span></h5>
            <p>
              Trikaya Media is a leading Publishing, Events and Media Marketing Company based in Mumbai. The company has taken great strides and become the backbone of major food-related events like FoodFood TV Channel Awards 2017, 2018 & 2019 and World Food India Food Street 2017 as well as being the exclusive sales, marketing and events agency for the world’s number one food magazine BBC GoodFood India. Trikaya Media has firmly established itself as one of the leaders in the food and hospitality event sector, having associated with leading hotel and restaurant brands.
            </p>
          </div>
        </div> -->

        <!-- <div class="row section-title">
          <div class="col-md-12 col-sm-6">
            <h5 class="text-uppercase text-center">ABOUT <span>KANIKA SETHI HOSPITALITY. WEDDINGS. EVENTS</span></h5>
            <h5 class="text-uppercase">ABOUT KANIKA SETHI HOSPITALITY. WEDDINGS. EVENTS</h5>
            <p>
              Kanika Sethi Hospitality. Weddings. Events is a leading experiential hospitality & event designing company based in New Delhi. It has created luxurious avant-garde celebrations and curated top-notch hospitality experiences across events like IIFA Awards, weddings and intellectual property events. The company has been involved in several sporting events like IPL, ISL, ICC T20 World Cup 2016 & Champion’s league T20 tournaments. With a diverse platter of events, it has become an impactful sutradhar in weaving together extraordinary celebratory experiences.
            </p>
          </div>
        </div> -->

        <div class="border-top my-3"></div>

        <div class="row justify-content-center mb-4">
            <div class="col-md-12 col-sm-6 section-title text-center">
                <h4 class="text-uppercase">some top brands <span>we bring home</span></h4>
                <!-- <h2 class="text-uppercase">some of our partners</h2> -->
            </div>
        </div>

        <!-- <div class="row justify-content-center pt-4">
          <div class="col-lg-12 col-md-12 client_logo">
              <ul class="list-inline align-center">
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/the_leela.png')}}" alt="The Leela">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Accor_logo.png')}}" alt="Accor">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/First-Fiddle-Restaurants.png')}}" alt="First Fiddle Restaurants">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/four-seasons-hotel-logo.png')}}" alt="Four Seasons hotel">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/hilton.png')}}" alt="Hilton">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/hyatt.png')}}" alt="Hyatt">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/itc-logo-mast.png')}}" alt="ITC">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Lite-Bite-Foods-1.png')}}" alt="Lite Bite Foods">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Marriott_Hotels__Resorts_Logo.png')}}" alt="Marriott Hotels Resorts">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Massive-Restaurants-2.png')}}" alt="Massive Restaurants">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Old-World-Hospitality-1.png')}}" alt="Old World Hospitality">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Olive-Bar-&-kitchen-1.png')}}" alt="Olive Bar & kitchen">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Qualia-1.png')}}" alt="Qualia">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Shangri-La_Hotels_and_Resorts_logo.png')}}" alt="Shangri-La Hotels and Resorts">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/Speciality-Restaurants-2.png')}}" alt="Speciality Restaurants">
                  </li>
                  <li class="list-inline-item">
                      <img src="{{asset('images/uploads/client_logo/TajHotelsPalacesResortsSafarisLogo.png')}}" alt="Taj Hotels Palaces Resorts Safaris">
                  </li>
              </ul>
          </div>
        </div> -->

        <div class="owl-carousel clients-carousel mb-4">
          <img src="{{asset('images/uploads/client_logo/the_leela.png')}}" alt="The Leela">
          <img src="{{asset('images/uploads/client_logo/Accor_logo.png')}}" alt="Accor">
          <img src="{{asset('images/uploads/client_logo/First-Fiddle-Restaurants.png')}}" alt="First Fiddle Restaurants">
          <img src="{{asset('images/uploads/client_logo/four-seasons-hotel-logo.png')}}" alt="Four Seasons hotel">
          <img src="{{asset('images/uploads/client_logo/hilton.png')}}" alt="Hilton">
          <img src="{{asset('images/uploads/client_logo/hyatt.png')}}" alt="Hyatt">
          <img src="{{asset('images/uploads/client_logo/itc-logo-mast.png')}}" alt="ITC">
          <img src="{{asset('images/uploads/client_logo/Lite-Bite-Foods-1.png')}}" alt="Lite Bite Foods">
          <img src="{{asset('images/uploads/client_logo/Marriott_Hotels__Resorts_Logo.png')}}" alt="Marriott Hotels Resorts">
          <img src="{{asset('images/uploads/client_logo/Massive-Restaurants-2.png')}}" alt="Massive Restaurants">
          <img src="{{asset('images/uploads/client_logo/Old-World-Hospitality-1.png')}}" alt="Old World Hospitality">
          <img src="{{asset('images/uploads/client_logo/Olive-Bar-&-kitchen-1.png')}}" alt="Olive Bar & kitchen">
          <img src="{{asset('images/uploads/client_logo/Qualia-1.png')}}" alt="Qualia">
          <img src="{{asset('images/uploads/client_logo/Shangri-La_Hotels_and_Resorts_logo.png')}}" alt="Shangri-La Hotels and Resorts">
          <img src="{{asset('images/uploads/client_logo/Speciality-Restaurants-2.png')}}" alt="Speciality Restaurants">
          <img src="{{asset('images/uploads/client_logo/TajHotelsPalacesResortsSafarisLogo.png')}}" alt="Taj Hotels Palaces Resorts Safaris">
        </div>

        <div class="border-top my-3"></div>        
    </div>
</section>
@endsection
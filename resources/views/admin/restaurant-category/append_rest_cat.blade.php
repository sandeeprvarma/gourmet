<div class="form-group row">
    <label class="col-sm-2 col-form-label">Restaurant list</label>
    <div class="col-sm-10">
        <select name="cat_id" id = "cat_id" class="form-control" required {{$disabled??''}}>
            <option value="">--Select Restaurant Category--</option>
            @foreach($restaurants as $rest)
            <option value="{{$rest->id}}" {{( !empty($cat_id) && $cat_id == $rest->id)?'selected':''}}>{{$rest->name}}</option>
            @endforeach
        </select>
    </div>
</div>
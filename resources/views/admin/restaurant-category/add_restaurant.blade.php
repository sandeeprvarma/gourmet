@extends('admin.layouts.app')

@section('content')
@include('admin.layouts.navigations')

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-header card">
                    <div class="card-block">
                        <h5 class="m-b-10">Add New Restaurant Category</h5>
                        <ul class="breadcrumb-title b-t-default p-t-10">
                            <li class="breadcrumb-item">
                                <a href="#"> <i class="fa fa-home"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Restaurant Category</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Add Category</a>
                            </li>
                            <!-- <a class="btn btn-success" href="{{ url('/admin/import_excel/restaurant_category/') }}" style="float: right;">Import</a> -->
                        </ul>
                    </div>
                </div>


                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-block">
                                    <h4 class="sub-title">Restaurant Category Details</h4>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                    </div>
                                    @endif
                                    @include('admin.restaurant-category.restaurant_category_form',['post_url'=>'/admin/restaurant-category','required'=>'required'])
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</div>
</div>
@endsection
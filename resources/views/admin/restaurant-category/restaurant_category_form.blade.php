<form method="POST" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'patch')
        @method('patch')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Category Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" value="{{ old('name') ?? ($res_catgories->name??'')}}" placeholder="Enter Restaurant Category  Name" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Category Display Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="logo" {{$required}} />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Common logo</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="c_logo" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Restaurant Category</label>
        <div class="col-sm-10">
            <select name="master_category" class="form-control">
                @foreach($categories as $category)
                    @if($category->id == 1 || $category->id == 2)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
    <!-- <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="description">{{ old('description') ?? ($res_catgories->description??'')}}</textarea>
        </div>
    </div> -->
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>
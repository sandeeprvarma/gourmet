<form method="POST" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'patch')
        @method('patch')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Restaurant Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" value="{{ old('name')??($restaurant->name??'') }}" placeholder="Enter Restaurant Name" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Display Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="image" {{$required}} />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Logo Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="logo" />
        </div>
    </div>
    
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Category</label>
        <div class="col-sm-10">
            <select name="master_cat" id="master_cat" required class="form-control">
                <option value="">--Select Category--</option>
                @foreach($categories as $category)
                <option value="{{$category->id}}" {{(isset($restaurant) && $restaurant->master_cat == $category->id)?'selected':''}}>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row" id="celeb_image" style="{{$hide??''}}">
        <label class="col-sm-2 col-form-label">Celebrity Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="celeb_image" />
        </div>
    </div>
    <input type="hidden" name="is_edit" value = "{{isset($restaurant) ? $restaurant->cat_id :''}}">
    <div id="rest_cat">
        @include('admin.restaurant-category.append_rest_cat' , ['disabled'=> $disabled, 'cat_id' => isset($restaurant->cat_id)?$restaurant->cat_id:''])
    </div>
    <!-- <div class="form-group row">
        <label class="col-sm-2 col-form-label">Restaurant list</label>
        <div class="col-sm-10">
            <select name="cat_id" id = "cat_id" class="form-control" required {{$disabled??''}}>
                <option value="">--Select Restaurant Category--</option>
                @foreach($restaurants as $rest)
                <option value="{{$rest->id}}" {{( isset($restaurant) && $restaurant->cat_id == $rest->id)?'selected':''}}>{{$rest->name}}</option>
                @endforeach
            </select>
        </div>
    </div> -->
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">SiteURL/Link</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="site_url" value="{{ old('site_url')??($restaurant->site_url??'') }}" placeholder="Enter Restaurant Site URL" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="description" required>{{ old('description')??($restaurant->description??'') }}</textarea>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>

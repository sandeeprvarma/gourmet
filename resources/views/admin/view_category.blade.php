@extends('admin.layouts.app')

@section('content')
@include('admin.layouts.navigations')
<div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-header card">
                                        <div class="card-block">
                                            <h5 class="m-b-10">View Categories</h5>
                                            <ul class="breadcrumb-title b-t-default p-t-10">
                                                <li class="breadcrumb-item">
                                                    <a href="#"> <i class="fa fa-home"></i> </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">Category</a>
                                                </li>
                                                <li class="breadcrumb-item">View Categories
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                    </div>
                                    @endif

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Restaurants Categories List</h5>                                                
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="simpletable" class="table table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Category Name</th>
                                                                        <th>Category Display Picture</th>
                                                                        <th>Common Logo</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($categories as $category)
                                                                    <tr>
                                                                        <td>{{$category->name}}</td>
                                                                        <td><img src="{{url($category->logo)}}" width="50px" height="50px"></td>
                                                                        @if(!empty($category->c_logo))
                                                                        <td><img src="{{url($category->c_logo)}}" width="50px" height="50px"></td>
                                                                        @else
                                                                        <td>-- NONE --</td>
                                                                        @endif
                                                                        <td>
                                                                            <a href="{{ url('/admin/edit-category/'.$category->id) }}"> <button class="btn btn-primary btn-outline-primary btn-icon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                                            </a>
                                                                            <a href="{{ url('/admin/delete-category/'.$category->id) }}"> <button class="btn btn-danger btn-outline-primary btn-icon" onclick="return confirm('Please confirm to delete?')" ><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                                            </a>                                   
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>                                                            
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="styleSelector">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
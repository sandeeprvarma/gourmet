@extends('admin.layouts.app')

@section('content')
@include('admin.layouts.navigations')

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-header card">
                    <div class="card-block">
                        <h5 class="m-b-10">Import {{ucfirst($model[$table])}}</h5>
                        <ul class="breadcrumb-title b-t-default p-t-10">
                            <li class="breadcrumb-item">
                                <a href="#> <i class="fa fa-home"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Import</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">{{ucfirst($model[$table])}}</a>
                            </li>
                        </ul>
                    </div>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-block">
                                    <h4 class="sub-title">Import data</h4>
                                    <form method="POST" action="{{ url('/admin/import_excel/'.$table) }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Excel File</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" name="excel_file" />
                                            </div>
                                        </div>
                                        <div class="row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
                                        </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                            <div class="card">
                                <div class="card-block">
                                    <h4 class="sub-title">Upload Images</h4>
                                    <form method="POST" action="{{ url('/admin/upload-images') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Select images or multiple images</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" name="image[]" required multiple />
                                            </div>
                                        </div>
                                        <div class="row">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
                                        </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>
</div>
@endsection
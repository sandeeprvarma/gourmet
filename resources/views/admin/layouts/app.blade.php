<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Admin</title>
    <!--[if lt IE 10]>
          <script src="{{ asset('admin_assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
          <script src="{{ asset('admin_assets/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')}}"></script>
          <![endif]-->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel='icon' href='favicon.ico' type='image/x-icon'/ >
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/icon/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/icon/icofont/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" href="{{ asset('admin_assets/assets/pages/chart/radial/css/radial.css')}}" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">    
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_assets/assets/css/style.css')}}">
</head>
<body class="fix-menu">
    <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
    @yield('content')
    <script src="{{ asset('admin_assets/bower_components/jquery/js/jquery.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/popper.js/js/popper.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/bootstrap/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('admin_assets/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>

    <script src="{{ asset('admin_assets/bower_components/modernizr/js/modernizr.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/modernizr/js/css-scrollbars.js')}}"></script>

    <script src="{{ asset('admin_assets/bower_components/i18next/js/i18next.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/js/common-pages.js')}}"></script>
    
    <script src="{{ asset('admin_assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/pages/data-table/js/jszip.min.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/pages/data-table/js/pdfmake.min.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/pages/data-table/js/vfs_fonts.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('admin_assets/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>

    <script src="{{ asset('admin_assets/assets/pages/data-table/js/data-table-custom.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/js/pcoded.min.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/js/vertical/vertical-layout.min.js')}}"></script>
    <script src="{{ asset('admin_assets/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="https://use.fontawesome.com/315a142228.js?v=1"></script>
    <script src="{{ asset('admin_assets/assets/js/script.js')}}"></script>
    
    <script type="text/javascript">
        $(document).on('change', '#master_cat', function() {

            var mas_id = $(this).val();
            var rc = $('input[name=is_edit]').val();
            $.ajax({
                type: 'POST',
                url: '/admin/append-rest-cat',
                data: {"_token": "{{ csrf_token() }}",mas_id: mas_id, rc: rc},
                success: function (response) {
                    console.log("reponse");
                    $("#rest_cat").html(response);
                }
            });
            
            /*if(mas_id == 1 || mas_id == 2) {
                $('#cat_id').prop('disabled', false);
            } else {
                 $('#cat_id').prop('disabled', 'disabled');
            }*/

            if(mas_id == 4 || mas_id == 3) {
                $('#celeb_image').show();
                $('#celeb_image').prop('required', true);
            } else {
                $('#celeb_image').hide();
                $('#celeb_image').prop('required', false);
            }
         });
    </script>
</body>
</html>

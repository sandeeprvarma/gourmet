<form method="POST" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @method($method)
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Category Name</label>
        <div class="col-sm-10">
            <input type="text" name = "name" value="{{old('name') ? old('name'): (isset($category->name)?$category->name:'') }}" class="form-control" placeholder="Enter Restaurant Category  Name" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Category Display Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="logo"/>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>

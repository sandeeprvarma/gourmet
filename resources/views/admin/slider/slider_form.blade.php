<form method="POST" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'patch')
        @method('patch')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Description</label>
        <div class="col-sm-10">
            <input type="text" name = "desc" value="{{old('desc') ? old('desc'): (isset($slider->desc)?$slider->desc:'') }}" class="form-control" placeholder="Enter Slider Description" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Image</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="image" {{$required}} />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Type</label>
        <div class="col-sm-10">
            <select name="slider_type" class="form-control">
                <option value="1" {{ !empty($slider->slider_type) ? ($slider->slider_type == 1 ? 'selected="selected"' : ''): '' }}>Home</option>
                <option value="2" {{ !empty($slider->slider_type) ? ($slider->slider_type == 2 ? 'selected="selected"' : ''): '' }}>About us</option>
                <option value="3" {{ !empty($slider->slider_type) ? ($slider->slider_type == 3 ? 'selected="selected"' : ''): '' }}>Slider</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Slider Rank</label>
        <div class="col-sm-10">
            <input type="text" name = "rank" value="{{old('rank') ? old('rank'): (isset($slider->rank)?$slider->rank:'') }}" class="form-control" placeholder="Enter Slider Rank" required />
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>

<form method="POST" action="{{ $post_url }}" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'PATCH')
        @method('PATCH')
    @endif
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Supplier Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="name" value="{{ old('name') ?? ($supplier->name??'')}}" placeholder="Enter Name" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Supplier Category</label>
        <div class="col-sm-10">
            <select name="parent_cat" class="form-control">
                @foreach($categories as $category)
                <option value="{{$category->id}}" {{ (!empty($supplier->cat_id) && $supplier->cat_id == $category->id) ? 'selected' : '' }} >{{$category->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Display Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="image" {{$required}} />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Profile Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="profile_image" />
        </div>
    </div>
    {{-- <div class="form-group row">
        <label class="col-sm-2 col-form-label">Celebrity Picture</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="celeb_image" />
        </div>
    </div> --}}
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Mobile</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="mobile" value="{{ old('mobile') ?? ($supplier->mobile??'')}}" placeholder="Mobile" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Alternate Mobile</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="alt_mobile" value="{{ old('alt_mobile') ?? ($supplier->alt_mobile??'')}}" placeholder="Alternate Mobile" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="email" value="{{ old('email') ?? ($supplier->email??'')}}" placeholder="Email" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Alternate Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="alt_email" value="{{ old('alt_email') ?? ($supplier->alt_email??'')}}" placeholder="Alternate Email" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Website URL</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="website_url" value="{{ old('website_url') ?? ($supplier->website_url??'')}}" placeholder="Website URL" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Redirect URL</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="redirect_url" value="{{ old('redirect_url') ?? ($supplier->redirect_url??'')}}" placeholder="Redirect URL" required />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Description</label>
        <div class="col-sm-10">
            <textarea class="form-control" name="desc" required>{{ old('desc') ?? ($supplier->desc??'')}}</textarea>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-2"></label>
        <div class="col-sm-10">
            <button type="submit" class="btn btn-primary m-b-0">Submit</button>
            <button type="reset" class="btn btn-warning m-b-0">Cancel</button>
        </div>
    </div>
</form>
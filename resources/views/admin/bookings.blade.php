@extends('admin.layouts.app')

@section('content')
@include('admin.layouts.navigations')
<div class="pcoded-content">
                        <div class="pcoded-inner-content">

                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-header card">
                                        <div class="card-block">
                                            <h5 class="m-b-10">Bookings</h5>
                                            <ul class="breadcrumb-title b-t-default p-t-10">
                                                <li class="breadcrumb-item">
                                                    <a href="#"> <i class="fa fa-home"></i> </a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">Bookings</a>
                                                </li>
                                                <li class="breadcrumb-item"><a href="#!">View Bookings</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('success'))
                                    <div class="alert alert-success alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                    </div>
                                    @endif

                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">

                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Suppliers List</h5>                                                
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="dt-responsive table-responsive">
                                                            <table id="simpletable" class="table table-bordered nowrap">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Email</th>
                                                                        <th>Phone</th>
                                                                        <th>Description</th>
                                                                        <th>Destination</th>
                                                                        <th>Date</th>
                                                                        <!-- <th>Time</th> -->
                                                                        <th>AM/PM</th>
                                                                        <th>No.of Guests</th>
                                                                        {{-- <th>Action</th> --}}
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($bookings as $booking)
                                                                    <tr>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->name}}</td>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->email}}</td>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->phone}}</td>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->desc}}</td>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->destination}}</td>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->bk_date}}</td>
                                                                        <!-- <td style="word-wrap: break-word; ">{{$booking->hour.":".$booking->mins." ".$booking->meridian}}</td> -->
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->meridian}}</td>
                                                                        <td style="word-wrap: break-word; white-space: normal !important;">{{$booking->pickup}}</td>
                                                                        {{-- <td>
                                                                            <form method="post" action="{{ url('/admin/suppliers/'.$booking->id) }}">
                                                                                @method('delete')
                                                                                @csrf
                                                                                <button type="submit" class="btn btn-primary btn-outline-primary btn-icon"><i class="icofont icofont-user-alt-3"></i></button>
                                                                            </form>
                                                                        </td> --}}
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>                                                            
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div id="styleSelector">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
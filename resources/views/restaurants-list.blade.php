@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row mt-2">
      <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="black-text" href="/">Home</a></li>
            <li class="breadcrumb-item active">{{$category->name}}</li>
          </ol>
        </nav>
      </div>
    </div>
</div>
<section style="padding-bottom: 60px; padding-top: 0;">
  <div class="container rest_in_hotel h-100">
    <div class="row mb-4">
      <div class="col-lg-12 col-md-12 text-uppercase text-center px-4">
        <p>{{$category->name}}</p>
      </div>
    </div>
    <div class="row">
      @if(!$restaurants->isEmpty())
        @foreach($restaurants as $restaurant)
        <div class="col-lg-4 col-md-4 p-4">
          <div class="d-flex align-items-center" style="height: 250px;background-color: #fff;">
            <a class="mx-auto d-block" href="{{ route('restaurant',[$restaurant->master_category, $restaurant->id]) }}"><img src="{{url($restaurant->logo)}}" class="img-fluid" style="width:300px;"></a>
          </div>
        </div>
        @endforeach
      @else
        <div class="col-lg-12 text-center empty_result">
          <div class="face">
            <div class="band">
              <div class="red"></div>
              <div class="white"></div>
              <div class="blue"></div>
            </div>
            <div class="eyes"></div>
            <div class="dimples"></div>
            <div class="mouth"></div>
          </div>

          <h1>Oops! No Category found!</h1>
          <a href="/">Go TO Homepage</a>
        </div>
      @endif
    </div>
  </div>
</section>
@endsection
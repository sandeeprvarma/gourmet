@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row mt-2">
      <div class="col-lg-12">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a class="black-text" href="/">Home</a></li>
            @if($sub_category)
              <li class="breadcrumb-item"><a class="black-text" href="{{ route('rest_subcat',$category->id)}}">{{$category->name}}</a></li>
              <li class="breadcrumb-item active">{{$sub_category->name}}</li>
            @else
              <li class="breadcrumb-item active">{{$category->name}}</li>
            @endif
          </ol>
        </nav>
      </div>
    </div>
</div>
<section id="restaurants" class="restaurants-info">
  <div class="container">
    <div class="row justify-content-center pb-5">
        <div class="col-lg-12 col-sm-12 text-center logo_holder">
          <div id="logo" style="padding: 10px;">
              @if($sub_category)
                <img class="card-img img-fluid rounded-0" src="{{url($sub_category->c_logo)}}" alt="post-thumb">
              @else
                <img class="card-img img-fluid rounded-0" src="{{url($category->c_logo)}}" alt="post-thumb">
              @endif
          </div>
        </div>
    @if(!$restaurants->isEmpty())
      @foreach($restaurants as $restaurant)
        <div class="col-lg-6 col-sm-6">
            <div class="card text-center back-color img_container rounded-0 mb-4">
              <img class="card-img img-fluid rounded-0" style="border: 1px solid #fff;" src="{{url($restaurant->image)}}" alt="post-thumb">
                <div class="card-footer text-center back-color text-uppercase"><a>{{$restaurant->name}}</a></div>
            </div>
            <div class="mb-4 text-center" style="border-bottom: 2px dotted #202020;">
                <!-- <img src="assets/img/suppliers_data/mdw.jpg" class="img-fluid" style="width: 150px;"> -->
                <p class="res_desc sd_desc">{{$restaurant->description}}</p>
                @if(!empty($restaurant->site_url))
                <p class="res_link">For more details, click on link: <a href="/client/{{base64_encode($restaurant->site_url)}}"> {{ $restaurant->site_url }}</a></p>
                @endif
                @if(!empty($restaurant->logo))
                <img src="{{url($restaurant->logo)}}" style="width: 150px;">
                @endif
                <div class="rin_bottom"></div>
            </div>
        </div>
      @endforeach
    @else
      <div class="col-lg-12 text-center empty_result">
        <div class="face">
          <div class="band">
            <div class="red"></div>
            <div class="white"></div>
            <div class="blue"></div>
          </div>
          <div class="eyes"></div>
          <div class="dimples"></div>
          <div class="mouth"></div>
        </div>

        <h1>Oops! No Restaurants in this category!</h1>
        <a href="/">Go TO Homepage</a>
      </div>
    @endif
    </div>
  </div>
</section>
@endsection
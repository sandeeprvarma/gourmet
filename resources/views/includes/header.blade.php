<header class="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar">
        <a class="navbar-brand" href="{{url('/')}}">
            <img src="{{ asset('images/final-logo.png') }}" style="width: 130px; height: 70px;" alt="">
        </a>

      <div class="navbar-nav-scroll">
        <ul class="navbar-nav bd-navbar-nav flex-row">
            <li class="nav-item">
                <a class="nav-link" id="hp" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="au" href="/about">About us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="bk" href="/booking">Booking</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="sv" href="/services">Suppliers</a>
            </li>
        </ul>
      </div>

<!--         <form action="#" class="subscribe-form ml-auto">
            <div class="form-group d-flex">
                <input type="text" class="form-control" placeholder="Search">
                <i class="fa fa-search fa-fw"></i>
                <input type="submit" value="Search" class="submit px-3">
            </div>
        </form> -->

      <form action="{{url('/search')}}" method="POST" id="sb_form" class="sb_form hassbi">
        @csrf
        <input id="sb_form_q" class="sb_form_q" name="search_val" type="text" title="Enter your search term" required="" oninvalid="this.setCustomValidity('Please type something to search')"
    oninput="this.setCustomValidity('')" />
        <input id="sb_form_go" class="sb_form_go" type="submit" title="Search" name="search" value="Search">
    </form>
</header>
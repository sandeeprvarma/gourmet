<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>Gourmet@Home</title>

<!-- Favicons -->
<link href="{{ asset('images/favicon.png')}}" rel="icon">
<link href="{{ asset('images/apple-touch-icon.png')}}" rel="apple-touch-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i" rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/icofont.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/boxicons.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/animate.min.css')}}" rel="stylesheet">
<link href="{{ asset('css/venobox.css')}}" rel="stylesheet">
<link href="{{ asset('css/owl.carousel.min.css')}}" rel="stylesheet">

<!-- Template Main CSS File -->
<link href="{{ asset('css/style.css')}}" rel="stylesheet">
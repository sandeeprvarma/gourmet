<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container">
        <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

            <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

            <div class="carousel-inner" role="listbox">

                <!-- Slide 1 -->
                <div class="carousel-item active">
                    <!-- <img src="assets/img/home_slider/bringing_home_mother_nature.jpg" class="img-fluid"> -->
                    <img src="{{asset('images/home_slider/Bringing-home-mother-nature.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Bringing Home Mother Nature</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slide 2 -->
                <div class="carousel-item">
                    <img src="{{asset('images/home_slider/Creating-bespoke-experiences.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Creating Bespoke Experiences.</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slide 3 -->
                <div class="carousel-item">
                    <img src="{{asset('images/home_slider/Making-dreams-come-true.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Making Dreams Come True.</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slide 4 -->
                <div class="carousel-item">
                    <img src="{{asset('images/home_slider/Personalising-every-preference.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Personalising Every Preference.</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Slide 5 -->
                <div class="carousel-item">
                    <img src="{{asset('images/home_slider/Recreating-vintage-era.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Recreating Vintage Era.</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Slide 6 -->
                <div class="carousel-item">
                    <img src="{{asset('images/home_slider/Reimagining-every-special-occasion.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Reimagining Every Special Occasion.</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Slide 7 -->
                <div class="carousel-item">
                    <img src="{{asset('images/home_slider/Serving-the-world-on-a-plate.png')}}" class="img-fluid" style="height: auto;  width: 100%;">
                    <div class="carousel-container">
                        <div class="carousel-content">
                            <h2 class="animated fadeInDown">Serving The World On A Plate.</h2>
                            <!-- <p class="animated fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus
                                et tempore modi architecto.</p> -->
                            <div>
                                <a href="#menu" class="btn-menu animated fadeIn">WHAT WE DO FOR YOU</a>
                                <!-- <a href="#book-a-table" class="btn-book animated fadeIn">Book a Table</a> -->
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>
</section>
<!-- End Hero Section -->
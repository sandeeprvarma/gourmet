<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');

Route::get('restaurant/{id}', 'API\RestaurantController@getRestaurantsListById');
Route::get('restaurants', 'API\RestaurantController@getRestaurants');
Route::get('restaurant-list/{id}', 'API\RestaurantController@getRestaurantsByCategory');
Route::get('categories', 'API\RestaurantController@getAllCategories');
Route::post('search', 'API\RestaurantController@searchResult');
Route::post('booking', 'API\RestaurantController@storeBookings');



// Route::middleware('auth:api')->group( function () {
//     Route::resource('products', 'API\ProductController');
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* HomeController Routes*/
Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@aboutUs');
Route::get('/contact', 'HomeController@contactUs');
Route::get('/terms', 'HomeController@termaConditions');
Route::get('/policy', 'HomeController@privacyPolicy');
Route::post('/search', 'HomeController@searchResult');

/* ServiceController Routes*/
Route::get('/services', 'ServiceController@index');
Route::get('service_providers/{id}', 'ServiceController@serviceProviders')->name('service_providers');
Route::get('supplier_info/{id}', 'ServiceController@supplierInfo')->name('supplier_info');

/* RestaurantController Routes*/
Route::get('rest_subcat/{id}', 'RestaurantController@restaurantSubCategory')->name('rest_subcat');
Route::get('restaurant/{mid}/{cid}', 'RestaurantController@restaurantList')->name('restaurant');

/* BookingController Routes*/
Route::get('/booking', 'HomeController@booking');
Route::post('/booking', 'HomeController@storeBookings');

Route::get('/client/{url}', function($url) {
	$url =  base64_decode($url);
	return view('load_restaurant')->with(['url'=>$url]);
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['prefix' => 'admin','middleware' => ['check_admin'],'namespace' => 'Admin'], function() {
	Route::get('/', 'ManageCategoryController@index');
	Route::get('/add-category', 'ManageCategoryController@create');
	Route::post('/add-category', 'ManageCategoryController@store');
	Route::get('/view-category', 'ManageCategoryController@index');
	Route::get('/edit-category/{id}', 'ManageCategoryController@edit');
	Route::post('/edit-category/{id}', 'ManageCategoryController@update');
	Route::get('/delete-category/{id}', 'ManageCategoryController@destroy');

	Route::resource('restaurant-category','RestaurantsCategoryController');
	Route::resource('restaurant','RestaurantsController');
	Route::resource('suppliers-category','SupplierCategoryController');
	Route::resource('suppliers','SupplierController');
	Route::resource('sliders','SliderController');

	Route::get('/import_excel/{table}', 'ImportExcelController@importForm');
	Route::post('/import_excel/{table}', 'ImportExcelController@import');

	Route::post('/upload-images', 'ImportExcelController@uploadImages');

	Route::get('/bookings', 'BookingsController@index');
	Route::post('/append-rest-cat', 'RestaurantsCategoryController@appendRestCat');
});
<?php 
    include_once("common.php");
?>
<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-header card">
                    <div class="card-block">
                        <h5 class="m-b-10">Add New Supplier</h5>
                        <ul class="breadcrumb-title b-t-default p-t-10">
                            <li class="breadcrumb-item">
                                <a href="index-2.html"> <i class="fa fa-home"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Services</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Suppliers</a>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-block">
                                    <h4 class="sub-title">Supplier Details</h4>
                                    <form>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Supplier Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Supplier Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Supplier Category</label>
                                            <div class="col-sm-10">
                                                <select name="select" class="form-control">
                                                    <option value="opt1">Men's Designerwear</option>
                                                    <option value="opt2">Women's Designerwear</option>
                                                    <option value="opt3">Make-up Artistes</option>
                                                    <option value="opt4">Jewellery</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Supplier Description</label>
                                            <div class="col-sm-10">
                                                <textarea rows="5" cols="5" class="form-control"
                                                    placeholder="Enter Supplier Description"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Supplier Image</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Supplier's Work Images</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Mobile/Phone No.</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Mobile/Phone Number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Alternate Mobile/Phone No.</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Alternate Mobile/Phone Number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Alternate Email</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Alternate Email">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Website URL</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Website URL">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Redirection URL</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" placeholder="Enter Redirection URL">
                                            </div>
                                        </div>                                        
                                        <div class="row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                                <button type="submit" class="btn btn-warning m-b-0">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div id="styleSelector">
        </div>
    </div>
</div>

</div>
</div>
</div>
</div>
<?php 
    include_once("footer.php");
?>
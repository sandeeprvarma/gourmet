<?php 
    include_once("common.php");
?>
<div class="pcoded-content">
						<div class="pcoded-inner-content">

							<div class="main-body">
								<div class="page-wrapper">

									<div class="page-header card">
										<div class="card-block">
											<h5 class="m-b-10">View Restaurants</h5>
											<ul class="breadcrumb-title b-t-default p-t-10">
												<li class="breadcrumb-item">
													<a href="#"> <i class="fa fa-home"></i> </a>
												</li>
												<li class="breadcrumb-item"><a href="#!">Restaurant</a>
												</li>
												<li class="breadcrumb-item"><a href="#!">View Restaurant</a>
												</li>
											</ul>
										</div>
									</div>


									<div class="page-body">
										<div class="row">
											<div class="col-sm-12">

												<div class="card">
													<div class="card-header">
														<h5>Restaurants List</h5>												
													</div>
													<div class="card-block">
														<div class="dt-responsive table-responsive">
															<table id="simpletable" class="table table-bordered nowrap">
																<thead>
																	<tr>
																		<th>Rest.Name</th>
																		<th>Rest.Category</th>
																		<th>Rest.Image</th>
																		<th>Rest.Logo</th>
																		<th>Rest.Descriptio</th>
																		<th>Redirection URL</th>
																		<th>Action</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>Bradley Greer</td>
																		<td>Software Engineer</td>
																		<td>London</td>
																		<td>41</td>
																		<td>2012/10/13</td>
																		<td>$132,000</td>
																		<td>
																			<button class="btn btn-primary btn-outline-primary btn-icon"><i class="icofont icofont-user-alt-3"></i></button>									
																		</td>
																	</tr>
																	<tr>
																		<td>Dai Rios</td>
																		<td>Personnel Lead</td>
																		<td>Edinburgh</td>
																		<td>35</td>
																		<td>2012/09/26</td>
																		<td>$217,500</td>
																		<td>
																			<button class="btn btn-primary btn-outline-primary btn-icon"><i class="icofont icofont-user-alt-3"></i></button>									
																		</td>
																	</tr>
																</tbody>															
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>

							<div id="styleSelector">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
    include_once("footer.php");
?>